//Fonction recherche prénom et nom, retourne true si trouvé
console.time();
let data = {
    'firstNames': [
        'Theau', 
        'Alice', 
        'Alexandre', 
        'Mathilde',
        'Allan',
        'Claudette'
    ],
    'lastNames': [
        'Dupoud',
        'Marcus',
        'Mazeron',
        'Coty',
        'Guillet',
        'Goncalves'
    ],
}

function searchInArray(first, last, data) {
    let findFirst = false;
    let findLast = false;
    
    for (let i = 0; i < data.firstNames.length; i++) {
        if(data.firstNames[i] == first) {
            findFirst = true;
            break;
        }  
    }
    
    for (let i = 0; i < data.lastNames.length; i++) {
        if(data.lastNames[i] == last) {
            findLast = true;
            // break;
        }  
    }
    
    
    if(findFirst && findLast) {
        return true;
    }
    
    return false;
}

searchInArray('Theau', 'Goncalves', data);


let welcomeDiv = document.createElement('div');
welcomeDiv.className = 'welcome-div text-center mt-3';
welcomeDiv.innerHTML = "Coucou les amis";

// document.querySelector('body').appendChild(welcomeDiv);

let $counterDiv = document.querySelector("#counter-person");
let $addBtn = $counterDiv.querySelector(".add");
let $resetBtn = $counterDiv.querySelector(".reset");

let $removeBtn = $counterDiv.querySelector(".remove");

let $counter = $counterDiv.querySelector("span");
let counter = localStorage.getItem('counter-person');

if(counter === null) {
    counter = 0;
}

$counter.innerHTML = counter.toString();

function updateCounter($htmlElement, counter) {
    $htmlElement.innerHTML = counter.toString();
    localStorage.setItem('counter-person', counter);
}

$addBtn.addEventListener('click', () => {
    counter++;
    updateCounter($counter, counter);
});

$removeBtn.addEventListener('click', () => {
    counter--;
    
    if(counter < 0) {
        counter = 0
    }
    
    updateCounter($counter, counter);
});

$resetBtn.addEventListener('click', () => {
    counter = 0;
    updateCounter($counter, counter);
});


let hobbies = ['Poney', 'Tringle', 'Livre', 'Dieu', 'Guillaume'];
/**
* Boucler sur le tableau avec un forEach
* Créer un element html avec JS
* L'afficher dans une liste
*/

let $list = document.querySelector('#hobbies');

hobbies.forEach((hobby) => {
    let li = document.createElement('li');
    li.innerHTML = hobby;
    $list.appendChild(li);
});

// console.log($list.querySelector('li'))
// console.log($list.querySelectorAll('li'))


let $lis = $list.querySelectorAll('li');

$lis.forEach((li) => {
    li.addEventListener('click', () => {
        li.remove();
    })
})

/**
 * Au click sur le bouton, ajouter une classe "show" sur la div 
 * .hb-collapse-content
 * 
 * En css, faire sauter la limite de hauteur grace à la classe
 * "show"
 * 
 * +++ Modifier le bouton en voir moins et refermer la collapse
 */

let $collapse = document.querySelector('.hb-collapse');
let $content = $collapse.querySelector('.hb-collapse-content');
let $btn = $collapse.querySelector('button');

$btn.addEventListener('click', () => {
    $content.classList.toggle('show');
});





let $clickerGame = document.querySelector('.clicker-container');
let $zone = $clickerGame.querySelector('.clicker-zone');
let $text = $clickerGame.querySelector('.clicker-zone > div');
let $timer = $clickerGame.querySelector('.timer');
let clickCounter = 0;
let seconds = 10;
let mode = 'waiting';

$zone.addEventListener('click', () => {
    if(mode!= 'finished' && !$zone.classList.contains('started')) {
        $zone.classList.add('started');
        mode = 'started';
    }

    if(mode == 'started') {
        clickCounter++;
        $text.innerHTML = clickCounter.toString();
    }
});

window.setInterval(() => {
    if($zone.classList.contains('started')) {
        seconds--;
        $timer.innerHTML = seconds.toString();

        if(seconds <= 0) {
            seconds = 0;
            $timer.innerHTML = seconds.toString();
            mode = 'finished';
            $zone.classList.remove('started');
            $timer.style.display = 'none';
            $text.innerHTML = getScoreMessage(clickCounter);
        }
    }

}, 1000);

function getScoreMessage(score) {
    if(score < 20) {
        return "Seulement " + score + ", tu es une limace ou quoi ?"
    } else if (score < 40) {
        return score + " en 10 secondes, peu de capacité"
    } else if (score < 60) {
        return score + ", Dans la moyenne"
    } else if (score < 80) {
        return score + ", Not bad !"
    } else if (score < 100) {
        return score + ", t'es un chef"
    } else if (score >= 100) {
        return score + ", OMG t tro for"
    } 
    
    return "Fa marphe pa"
}

let $form = document.querySelector('#email-generator');

$form.addEventListener('submit', (e) => {
    e.preventDefault();
    let $inputs = $form.querySelectorAll('input');
    let mail = $inputs[0].value + '.' + $inputs[1].value + '@hb.com';
    let result = $form.querySelector('.hb-form-result');

    if(result === null) {
        result = document.createElement('div');
        result.classList.add('hb-form-result');
    } 
    result.innerHTML = mail.toLowerCase();
    $form.appendChild(result);
})


//Faire input type texte, et à la soumission du form
//Check si plus long que 9 lettres/chiffre
//Si pas le cas, afficher un message en dessou du champ

let $formPassword = document.querySelector('#password-check');
let $password = $formPassword.querySelector('#password');

$formPassword.addEventListener('submit', (e) => {
    e.preventDefault();
    cleanFormErrors(e.target);
    const regex = /\d/;
    
    
    if($password.value.length < 10 ) {
        displayError($password, 'Le mot de passe doit faire au moins 10 caractères')
    }

    if(!regex.test($password.value)) {
        displayError($password, 'Le mot doit contenir au moins un chiffre')
    }
});

function cleanFormErrors($htmlFormElement) {
    let $alerts = $htmlFormElement.querySelectorAll('.alert');

    $alerts.forEach(($alert) => {
        $alert.remove();
    });
}

function displayError($input, message) {
    let $error = document.createElement('div');
    $error.className = "alert alert-danger";
    $error.innerHTML = message;
    $input.after($error);
}
console.timeEnd();