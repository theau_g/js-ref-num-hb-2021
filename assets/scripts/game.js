let $box = document.querySelector('#box');
let $food = document.querySelector('#food');

//elementPosition = [X,Y]
let boxPosition = [0,0];
let foodPosition = [100,100];

let boxSize = 30;
let boxSpeed = 10;
let maxWidth = window.innerWidth - boxSize;
let maxHeight = window.innerHeight - boxSize;

let pressedKey = [];

initGame();

document.addEventListener("keydown", (event) => {

    if(!pressedKey.includes(event.key)) {
        pressedKey.push(event.key);
    }
    
    if (pressedKey.includes('ArrowRight')) {
        boxPosition[0] += boxSpeed;
    }

    if (pressedKey.includes('ArrowLeft')) {
        boxPosition[0] -= boxSpeed;
    }

    if (pressedKey.includes('ArrowDown')) {
        boxPosition[1] += boxSpeed;
    }

    if (pressedKey.includes('ArrowUp')) {
        boxPosition[1] -= boxSpeed;
    }
    moveElement($box, boxPosition);

});

document.addEventListener("keyup", (event) => {
    if(pressedKey.includes(event.key)) {
        const index = pressedKey.indexOf(event.key);
        pressedKey.splice(index, 1);
        
    }
});

function checkPosition() {
    if(boxPosition[0] < 0) {
        boxPosition[0] = 0;
    }

    if(boxPosition[0] > maxWidth) {
        boxPosition[0] =  maxWidth;
    }

    if(boxPosition[1] < 0) {
        boxPosition[1] = 0;
    }

    if(boxPosition[1] > maxHeight) {
        boxPosition[1] =  maxHeight;
    }
}

function generatePosition() {
    let x = Math.random() * maxWidth;
    let y = Math.random() * maxHeight;

    return [
        Math.floor(x),
        Math.floor(y),
    ];
}


function initGame() {
    boxPosition = generatePosition();
    foodPosition = generatePosition();

    console.log(foodPosition);
    console.log(getFoodCenter());


    moveElement($box, boxPosition);
    moveElement($food, foodPosition);

    $box.classList.remove('d-none');
    $food.classList.remove('d-none');
}

/**
 * @param {HTMLDivElement} $htmlElement 
 * @param {array} coordinates 
 */
function moveElement($htmlElement, coordinates) {
    checkPosition();
    $htmlElement.style.left = coordinates[0] + 'px';
    $htmlElement.style.top = coordinates[1] + 'px';

    if(canBeEat()) {
        foodPosition = generatePosition();
        moveElement($food, foodPosition);
    }
    
}

function getFoodCenter() {
    let coordinates = [];
    coordinates.push(foodPosition[0] + boxSize/2);
    coordinates.push(foodPosition[1] + boxSize/2);

    return coordinates;
}

function canBeEat() {

    let center = getFoodCenter();

    if(
        center[0] > boxPosition[0] 
        && 
        center[0] < (boxPosition[0] + boxSize)
        &&
        center[1] > boxPosition[1] 
        && 
        center[1] < (boxPosition[1] + boxSize)
    ) {
        console.log('coucou');
        return true;
    }
    return false;
}