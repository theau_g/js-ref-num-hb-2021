let $counterContainer = $('#counter');

let $counterSpan = $counterContainer.find('span');

$counterContainer.find('button').on('click', () => {

    $('p').fadeIn(2000);

    let counter = parseInt($counterSpan.text());
    counter++;
    $counterSpan.text(counter);
});

let $words = $('p');

$words.each(function() {
    $(this).text($(this).text() + ' toto');
    $(this).css('color', 'red');
});

$words.on({
    click: function() {
        $(this).css('background', 'blue');
    },

    dblclick: function() {
        $(this).css('background', 'green');
    }
});

// let vanillaWords = document.querySelectorAll("p");

// vanillaWords.forEach((word) => {
//     word.addEventListener('click', () => {
//         word.style.backgroundColor = 'blue';
//     })

//     word.addEventListener('dblclick', () => {
//         word.style.backgroundColor = 'green';
//     })
// })


// $words.animate({
//     'font-size' : '48px',
// }, 10000); // second exemple avec 1000ms (= 1s)


let collapse = $('.hb-collapse');
collapse.find('button').on('click', function () {
    collapse.find('.hb-collapse-content').toggleClass('show');
});