let classTotalCount = 13;
let actualCLassCount = 13;

//console.log('votre texte qui dit coucou');

// if(classTotalCount == actualCLassCount) {
//   console.log('On est pile poil');
// } else if(classTotalCount > actualCLassCount) {
//   console.log('Il manque du monde');
// } else {
//   console.log('Il y a un ou plusieurs imposteur(s)');
// }


// -----------------------------------------------------

//Etat de l'eau (Solide, liquide, gazeux)

let waterTemp = 99.99999;

// if(waterTemp <= 0) {
//   console.log("Il fait froid");
// } else if(waterTemp > 0 && waterTemp < 100) {
//   console.log("Tranquille liquide");
// } else {
//   console.log("C'est chaud");
// }


//Systeme de mode

let mode = 'sdkjofdn,o'; //Tester avec soft, hard, jdip, fhdsd

let exChaine = mode + ' coucou';
//Pour chaque mode, écrire : "Je suis en mode $var$" sauf si
//le mode n'est pas default, soft ou hard

// if(mode == 'default' || mode == 'soft' || mode == 'hard') {
//   console.log("Je suis en mode " + mode);
// } else {
//   console.log("Le mode " + mode + " n'existe pas");
// }


function getImc(height, weight) {
    squareHeight = height ** 2;
    
    return weight / squareHeight;
}

let imc = getImc(1.80, 85);

/**
* Calcul le bpm max à ne pas dépasser pendant l'effort
* @param {number} age 
* @return {number}
*/
function getMaxHeartRate(age) {
    return 207 - 0.7 * age
}

// console.log(getMaxHeartRate(24));

/**
* 
* @param {number} age 
* @param {number} bpm batements par minute pendant l'effort
* @return {string}
*/
function isThisDangerous(age, bpm) {
    if(bpm > getMaxHeartRate(age)) {
        return "ça ne va pas";
    } else {
        return "ça vaaaaaaa";
    }
}

/**
* Faire une fonction qui nous dit "ok tout va bien" lorsque
* notre imc est en dessous de 25
* @param {number} height 
* @param {number} weight 
* @return {string}
*/
function imcComment(height, weight) {
    if(getImc(height, weight) > 25) {
        return "va courir";
    } else {
        return "tu peux encore manger des chips";
    }
}

// console.log(imcComment(1.80,85));

//Déclaration de tableau
let notes = [8,16,12,10];
// console.log(notes);

//Afficher une case en particulier
// console.log(notes[5]);


notes.push(99);
// console.log(notes);

notes.push(1,2,3);
// console.log(notes);

notes.pop();
// console.log(notes);

notes.shift();
// console.log(notes);

//Réaffectation de valeur.
notes[3] = 'Toto';
// console.log(notes);

//Suppression de case : 
delete notes[2];
// console.log(notes);


// ========================================================
// Les boucles

for(let i = 0; i < 10; i++) {
    // console.log('Mon nombre est ' + i);
}

let names = ['Chloé', 'Pascal', 'Theau', 'Cunégonde', 'Perceval'];

for(let i = 0; i < names.length; i++) {
    // console.log(names[i]);
}

//Exo = 

const notesRefNum = [12,8,17,8,15,16,17];

let sum = 0;

for(let i = 0; i < notesRefNum.length; i++) {
    sum += notesRefNum[i];
}

const result = Math.round((sum / notesRefNum.length)*100) / 100;

// console.log(result);

//Diviser la somme par le nombre de note

/**
 * Exo -> Afficher tout les multiple de 7 qui sont 
 * plus petit que 300
 */

let multipleSept = [];

for (let i = 7; i < 300; i += 7) {
    multipleSept.push(i);
}

// console.log(multipleSept);
// console.log(multipleSept.length);

let cousins = ['Chloé', 'Pascal', 'Theau', 'Cunégonde', 'Perceval', null];

delete cousins[3];

console.log(cousins);

// for(let i = 0; i < cousins.length; i++) {
//     if(cousins[i] != undefined) {
//         console.log(cousins[i]);
//     }
// }

cousins.forEach((name, index) => {
    console.log(name + ' en case ' + index);
});


const notesRefNum2 = [12,8,17,82,15,16,17];

let max = notesRefNum2[0];

//Avec forEach, afficher la note la plus haute
notesRefNum2.forEach((nb) => {
    if(nb > max) {
        max = nb;
    }
});

console.log("La note max est " + max);

let prenoms = ['Chloé', 'Pascal', 'Theau', 'Cunégonde', 'Perceval', 'Jean-Michel'];
//.length

let maxLength = prenoms[0].length;
let maxLengthIndex = 0;

prenoms.forEach((prenom, index) => {
    if(prenom.length > maxLength) {
        maxLength = prenom.length;
        maxLengthIndex = index;
    }
});






console.log(prenoms[maxLengthIndex]);