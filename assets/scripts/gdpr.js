document.addEventListener('DOMContentLoaded', () =>{

    let $gdprModule = document.querySelector('#gdpr-module');
    let analyticsAcceptKey = 'gdpr_accept_status';

    //Changer le numéro de tracking par le votre
    let analyticsKey = 'G-6S2H9RN4S5';

    console.log(localStorage.getItem(analyticsAcceptKey));

    if(localStorage.getItem(analyticsAcceptKey) === null) {
        $gdprModule.style.display = "block";
    } else {
        
        if(
            localStorage.getItem('gdpr_analytics_accept_status') == 'ok' 
            ||
            localStorage.getItem(analyticsAcceptKey) == 'all') {
            loadAnalytics();
        }

        
    }


    $gdprModule.querySelector('.config-button').addEventListener('click', () => {
        $gdprModule.querySelector('.trackers-choices').style.display = 'block';
    });

    $gdprModule.querySelector('#accept-btn').addEventListener('click', () => {
        localStorage.setItem(analyticsAcceptKey, 'all');
        $gdprModule.style.display = 'none';
    });

    $gdprModule.querySelector('#config_accept').addEventListener('click', () => {
        if($gdprModule.querySelector('#analytics-checkbox').checked) {
            localStorage.setItem('gdpr_analytics_accept_status', 'ok');
        }

        localStorage.setItem(analyticsAcceptKey, 'filled');
        $gdprModule.style.display = 'none';
    });



function loadAnalytics() {
    const script = document.createElement('script');
    script.src = "https://www.googletagmanager.com/gtag/js?id=" + analyticsKey;
    script.addEventListener('load', () => {
        (window).dataLayer = (window).dataLayer || [];

        function gtag(...args) {
            (window).dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', analyticsKey);
    });

    document.querySelector('body').appendChild(script);
}



});