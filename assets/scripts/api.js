document.addEventListener('DOMContentLoaded', () => {
    
    fetch('https://api.spacexdata.com/v4/crew/', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
    .then(response => response.json())
    .then((data) => {

        let $crewList = document.querySelector("#spacex-crew-list");
        let $crewModel = document.querySelector("#spacex-crew-model");

        data.forEach((astro) => {
            let id = 'astro-' + astro.id;
            let $cloneModel = $crewModel.cloneNode(true);
            $cloneModel.id = id;
            $crewList.appendChild($cloneModel);

            let $astroCard = $crewList.querySelector('#' + id);
            $astroCard.querySelector('.card-title').innerHTML = astro.name;
            $astroCard.querySelector('.card-text').innerHTML = astro.agency;
            $astroCard.querySelector('.card-img-top').setAttribute('src', astro.image);
            $astroCard.classList.remove('d-none');
    
        });
    });

    
    fetch('https://api.spacexdata.com/v4/rockets', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
    .then(response => response.json())
    .then((rockets) => {

        let $rocketList = document.querySelector("#spacex-rockets-list");
        let $rocketModel = document.querySelector("#spacex-rocket-model");

        rockets.forEach((rocket) => {
            let id = 'rocket-' + rocket.id;
            let $cloneModel = $rocketModel.cloneNode(true);
            $cloneModel.id = id;
            $rocketList.appendChild($cloneModel);

            let $astroCard = $rocketList.querySelector('#' + id);
            $astroCard.querySelector('.card-title').innerHTML = rocket.name;
            $astroCard.querySelector('.card-text').innerHTML = rocket.description;
            $astroCard.querySelector('.card-img-top').setAttribute('src', rocket.flickr_images[0]);
            
            let collapseId = 'rocket-collapse-' + rocket.id;
            $astroCard.querySelector('a[data-bs-toggle="collapse"]').setAttribute('href', '#' + collapseId )
            $astroCard.querySelector('.collapse').id = collapseId;

            let $details = $astroCard.querySelector('.details');

            let height = document.createElement('li');
            height.innerHTML = 'Hauteur : ' + rocket.height.meters + 'm';
            $details.appendChild(height);
            
            
            $astroCard.classList.remove('d-none');

            
    
        });
    });


    //Haut, haut, bas, bas, gauche, droite, gauche, droite, B, A
    var k = [
        'ArrowUp', 
        'ArrowUp', 
        'ArrowDown', 
        'ArrowDown', 
        'ArrowLeft', 
        'ArrowRight', 
        'ArrowLeft', 
        'ArrowRight', 
        'b',
        'a',
    ],
    n = 0;
    document.addEventListener("keydown", (event) => {       
        if (event.key === k[n++]) {
            if (n === k.length) {
                document.body.style.backgroundColor = 'red';
                new Audio('assets/sounds/noel.mp3').play();
                n = 0;
            }
        }
        else {
            n = 0;
        }
    });
});

//Changer la couleur de fond + lancer musique de noel



