let slider = tns({
    container: '.my-slider',
    items: 1,
    autoplay: true, 
    autoplayButton: false,
    autoplayButtonOutput: false,
    navPosition: 'bottom',
    controlsText: ['<', '>'],
    mouseDrag: true,
    responsive: {
        "596": {
          "items": 2
        },
        "767": {
          "items": 3
        }
      },
});

// slider.events.on('transitionStart', () => {
//     console.log('oui');
// })


let cardSlider = tns({
    container: ".slider-card",
    items: 1,
    navPosition: 'bottom',
    autoplayButtonOutput: false,
    autoplay: true,
    controls: false,
    gutter: 20,
    responsive: {
        "596": {
          items: 2
        },
        "767": {
          items: 3
        },
        "992": {
            items: 4,
        }
      },
});


document.addEventListener('DOMContentLoaded', () => {

    let frenchFormat = 'DD/MM/YYYY HH[h]mm';
    
    // console.log(moment().format(frenchFormat));

    let newWorld = new Date('2021-08-31T17:00:00');
    let today = new Date();
    // console.log(moment(newWorld).format(frenchFormat))

    const moment = MomentRange.extendMoment(Moment);

});





