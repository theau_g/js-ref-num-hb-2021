/**
 * Fonction qui prend en paramètre un age et qui
 * retourne true si on est majeur, sinon false 
 * @param {number} age 
 * @return {boolean}
 */
function isMajor(age) {
    if(age >= 18) {
        return true;
    } else {
        return false;
    }
}

/**
 * Fonction qui calcule le nombre de jours vécus
 */
function getYourLifeTotalDay(age) {
    // Calcule le nombre de jour
    let totalDay = age * 365.25; 

    //Arrondi le nombre du dessus
    let roundTotalDay = Math.round(totalDay);

    //Retourne le nombre arrondi
    return roundTotalDay;
    
    //On pourrait aussi écrire :
    //return Math.round(age * 365.25);
}

// console.log(getYourLifeTotalDay(24));

/**
 * La fonction calcule la somme du tableau
 */

let notes = [12,14,8,11,19,17.5];

/**
 * Calcule la somme du tableau
 * @param {array} tab 
 * @return {number} 
 */
function getArraySum(tab) {
    let sum = 0;

    tab.forEach((value) => {
        sum += value;
    });

    return sum;
}

// console.log(getArraySum(notes));

/**
 * Écrire une fonction qui retourne un numéro de téléphone 
 * français à 10 chiffres formatté en un numéro international 
 * sous le format suivant : +33 6 XX XX XX XX. Tester avec 10 
 * numéros;
 */


let phone = '0665125234';
phone = '0473125234';

function internationalPhoneFormat(tel) {
    if(tel.charAt(0) === '0') {
        tel = tel.substring(1);
    }

    return '+33' + tel;
}
// console.log(internationalPhoneFormat(phone));

/**
 * Écrire une fonction qui permet de vérifier si un mot est un
 * palindrome. Retourne vraie si c'est le cas.
 * Tester cette fonction sur 10 mots.
 */


function isPalindrome(word) {
    let lowerCase = word.toLowerCase();
    let inversedString = lowerCase.split('').reverse().join('');

    if(lowerCase === inversedString) {
        return true;
    } 
    
    return false;
}

isPalindrome('Anna');

let $input = document.querySelector('#palindrome-input');
let $resultDiv = document.querySelector('#result');


$input.addEventListener('change', () => {
   $resultDiv.classList.remove('d-none');

   if(isPalindrome($input.value)) {
    $resultDiv.innerHTML = "Vrai";
   } else {
    $resultDiv.innerHTML = "Faux";
   }
   
});


/**
 * Generation mail
 */

let data = {
    'firstNames': [
        'Theau', 
        'Alice', 
        'Alexandre', 
        'Mathilde',
        'Allan',
        'Claudette'
    ],
    'lastNames': [
        'Dupoud',
        'Marcus',
        'Mazeron',
        'Coty',
        'Guillet',
        'Goncalves'
    ],
    'domains': [
        'live.fr',
        'gmail.com',
        'orange.fr',
        'wanadoo.fr',
        'drosalys.fr'
    ],
}

function randomValue(tab) {
    return tab[Math.floor(Math.random()*tab.length)];
}

function generateEmail(dataArray) {
    let choosenFirstName = randomValue(dataArray.firstNames);
    let choosenLastName = randomValue(dataArray.lastNames);
    let choosenDomain = randomValue(dataArray.domains);
    

    let format = Math.floor(Math.random()*3);

    let mail = null;

    if(format === 0) {
        let randomNumber = Math.floor(Math.random()*100) + 1;
        mail = choosenFirstName + randomNumber;
    } else if(format === 1) {
        mail = choosenFirstName.substring(0,1) + choosenLastName;
    } else {
        mail = choosenFirstName + '.' + choosenLastName;
    }

    return (mail + '@' + choosenDomain).toLowerCase();
}

// console.log(generateEmail(data));

function arrayRand(array) {
    return array[Math.floor(Math.random() * array.length)];
}



if(localStorage.getItem('first_log_check') === null) {
    let $firstLogContainer = document.querySelector('#first-time');
    $firstLogContainer.classList.remove('d-none');

    let btn = $firstLogContainer.querySelector('button');

    btn.addEventListener('click', () => {
        localStorage.setItem('first_log_check', 'yes');
        $firstLogContainer.classList.add('d-none');
    });
} 

let audio = new Audio('assets/sounds/leo.mp3');

document.querySelector('#play-sound').addEventListener('click', () => {
    audio.play();
});

document.querySelector('#pause-sound').addEventListener('click', () => {
    audio.pause();
});