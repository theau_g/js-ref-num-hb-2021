<?php

function getHeader(string $title = "Bienvenue sur mon site"): void
{
    include 'partials/base/header.php';
}

function getFooter(): void
{
    include 'partials/base/footer.php';
}