<section class="newsletter bg-dark text-light py-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <form action="">
                    Inscription à notre newsletter
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Votre email"
                               aria-label="Recipient's username" aria-describedby="button-addon2">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                            S'inscrire
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
